package com.company;


import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        ArrayList<Integer> list=new ArrayList<Integer>();
        ArrayList<Integer> list_odd=new ArrayList<Integer>();
        ArrayList<Integer> list_even=new ArrayList<Integer>();

        Random temp=new Random();

        for(int x=0;x<100;x++)
            list.add(temp.nextInt(100));

        list.forEach(item -> {
            if (item % 2==0)
                list_even.add(item);
            else
                list_odd.add(item);
        });

         System.out.println(list.toString());
         System.out.println(list_even.toString());
         System.out.println(list_odd.toString());

    }
}
